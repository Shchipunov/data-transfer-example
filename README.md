# Data transfer example#

This example demonstrates one of ways to transfer (copy) data from one database to another.

### Project description ###
The project features: 

* Analyzes and creates the same structure of a table if it's absent in destination database
* Converts data types to fit destination database
* Uses batch operations to transfer data

The project uses:

* JDBC