package com.shchipunov.workshop;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.shchipunov.workshop.ConnectionFactory.ConnectionType.MICROSOFT_SQL_SERVER;
import static com.shchipunov.workshop.ConnectionFactory.ConnectionType.MYSQL;
import static com.shchipunov.workshop.ConnectionFactory.ConnectionType.POSTGRESQL;
import static java.util.Objects.requireNonNull;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.shchipunov.workshop.ConnectionFactory.ConnectionType;

/**
 * Presents an utility that allows to transfer (copy) data from one database to
 * another
 * 
 * @author Shchipunov Stanislav
 */
public class DataTransferUtility {

	private static final int FETCH_SIZE = 100;
	private static final int BATCH_SIZE = 100;

	private DataTransferUtility() {

	}

	/**
	 * Copies data of a table from one database to another. If the table in
	 * destination database does not exist - it creates required structure of
	 * the table and only after that copies all data.
	 * 
	 * @param originTableName
	 * @param originConnection
	 * @param destinationTableName
	 * @param destinationConnection
	 */
	public static void copyTableFromOneDatabaseToAnother(String originTableName, Connection originConnection,
			String destinationTableName, Connection destinationConnection) {
		/* Check arguments on null / empty value */
		requireNonNull(originConnection);
		requireNonNull(destinationConnection);
		checkArgument(!isNullOrEmpty(originTableName), "Origin table name must not be null or empty!");
		checkArgument(!isNullOrEmpty(destinationTableName), "Destination table name must not be null or empty!");

		/*
		 * Checks table names on SQL injection attack. Because these strings
		 * must contain only a table name (one word) in this case it will be
		 * sufficient to use simple approach.
		 */
		if (!checkForSqlInjection(originTableName)) {
			throw new IllegalArgumentException(
					"Table name " + originTableName + " contains invalid values (must be one word with no spaces)!");
		}
		if (!checkForSqlInjection(destinationTableName)) {
			throw new IllegalArgumentException("Table name " + destinationConnection
					+ " contains invalid values (must be one word with no spaces)!");
		}

		Map<ConnectionType, Boolean> autoCommit = new HashMap<>(2);
		try {
			/* Stores the initial value to restore it later in finally block. */
			ConnectionType originConnectionType = getDatabaseProductName(originConnection);
			ConnectionType destinationConnectionType = getDatabaseProductName(destinationConnection);
			autoCommit.put(originConnectionType, originConnection.getAutoCommit());
			autoCommit.put(destinationConnectionType, destinationConnection.getAutoCommit());

			/* Sets auto commit to false value to handle transaction scope. */
			originConnection.setAutoCommit(false);
			destinationConnection.setAutoCommit(false);

			/* Uses try-with-resources to not handle closing manually. */
			try (Statement statement = originConnection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);) {
				/*
				 * Gives a hint as to the number of rows that should be fetched
				 * from the database when more rows are needed.
				 */
				statement.setFetchSize(FETCH_SIZE);
				try (ResultSet resultSet = statement.executeQuery("SELECT * FROM " + originTableName);) {
					/*
					 * To guarantee that both tables are identically (have both
					 * the same order of columns and the same types), should be
					 * inspected (by getting meta-data of tables).
					 */
					DatabaseMetaData databaseMetaData = destinationConnection.getMetaData();
					try (ResultSet tables = databaseMetaData.getTables(null, null, destinationTableName, null);) {
						if (!tables.next()) {
							StringBuilder createTableQuery = new StringBuilder();
							createTableQuery
									.append("CREATE TABLE ")
									.append(destinationTableName)
									.append(" (");
							/* Gets a description of the original table. */
							try (Statement describeStatement = originConnection.createStatement()) {
								ResultSet describeResultSet = describeStatement
										.executeQuery("DESCRIBE " + originTableName);
								List<String> strings = new ArrayList<>();
								while (describeResultSet.next()) {
									strings.add(describeResultSet.getString(1) + " " + describeResultSet.getString(2));
								}
								/*
								 * Converts the data types of the origin table
								 * into the data types of the destination table.
								 */
								String convertedData = convertDataTypes(originConnectionType, destinationConnectionType,
										strings);
								createTableQuery
										.append(convertedData)
										.append(")");
							}
							/* Creates non-existent table. */
							try (Statement createStatement = destinationConnection.createStatement()) {
								createStatement.execute(createTableQuery.toString());
							}
						}
					}
					/*
					 * Prepares the query for copying the table from one database to
					 * another.
					 */
					StringBuilder insertQuery = new StringBuilder();
					insertQuery
							.append("INSERT INTO ")
							.append(destinationTableName)
							.append(" VALUES")
							.append(" (");
					final int columnCount = resultSet.getMetaData().getColumnCount();
					for (int i = 1; i <= columnCount; i++) {
						insertQuery
								.append("?")
								.append(i != columnCount ? ", " : ")");
					}

					/*
					 * Prepared statement can help this method performs better
					 * because if the same SQL has to be run more than one time,
					 * it has to be compiled only once.
					 */
					try (PreparedStatement preparedStatement = destinationConnection
							.prepareStatement(insertQuery.toString());) {
						int count = 0;
						while (resultSet.next()) {
							for (int i = 1; i <= columnCount; i++) {
								preparedStatement.setObject(i, resultSet.getObject(i));
							}
							preparedStatement.addBatch();
							if (++count % BATCH_SIZE == 0) {
								preparedStatement.executeBatch();
							}
						}
						preparedStatement.executeBatch();
						destinationConnection.commit();
					} catch (SQLException exception) {
						/*
						 * Catch exception to roll back the current transaction.
						 */
						for (Throwable throwable : exception) {
							throwable.printStackTrace();
						}
						destinationConnection.rollback();
					}
				}
			}
		} catch (SQLException exception) {
			for (Throwable throwable : exception) {
				throwable.printStackTrace();
			}
		} finally {
			if (originConnection != null) {
				try {
					if (autoCommit.get(MYSQL) != null) {
						originConnection.setAutoCommit(autoCommit.get(MYSQL));
					}
					originConnection.close();
				} catch (SQLException exception) {
					exception.printStackTrace();
				}
			}
			if (destinationConnection != null) {
				try {
					if (autoCommit.get(POSTGRESQL) != null) {
						destinationConnection.setAutoCommit(autoCommit.get(POSTGRESQL));
					}
					destinationConnection.close();
				} catch (SQLException exception) {
					exception.printStackTrace();
				}
			}
		}
	}

	/* Inner auxiliary methods */

	private static boolean checkForSqlInjection(String... arguments) {
		return Stream.of(arguments).noneMatch(string -> string.contains("\\s"));
	}

	private static ConnectionType getDatabaseProductName(Connection connection) throws SQLException {
		String databaseProductName = connection.getMetaData().getDatabaseProductName();
		ConnectionType connectionType = ConnectionType.valueOf(databaseProductName.toUpperCase());
		if (connectionType != MYSQL || connectionType != POSTGRESQL || connectionType != MICROSOFT_SQL_SERVER) {
			throw new UnsupportedOperationException(String.format("Unsupported database: %s", databaseProductName));
		}
		return connectionType;
	}

	private static String convertDataTypes(ConnectionType from, ConnectionType to, List<String> strings) {
		if (from == to) {
			return strings.stream().collect(Collectors.joining(", "));
		}
		if (to == POSTGRESQL) {
			return strings.stream()
					.map(string -> string.replaceAll(" int.+", " int"))
					.map(string -> string.replaceAll(" tinyint\\(1\\)", " boolean"))
					.map(string -> string.replaceAll(" mediumtext", " varchar"))
					.map(string -> string.replaceAll(" datetime", " timestamp"))
					.collect(Collectors.joining(", "));
		} else if (to == MICROSOFT_SQL_SERVER) {
			return strings.stream()
					.map(string -> string.replaceAll(" int.+", " int"))
					.map(string -> string.replaceAll(" tinyint\\(1\\)", " bit"))
					.map(string -> string.replaceAll(" float", " real"))
					.map(string -> string.replaceAll(" mediumtext", " varchar"))
					.map(string -> string.replaceAll(" longtext", " varchar"))
					.map(string -> string.replaceAll(" mediumblob", " varbinary"))
					.map(string -> string.replaceAll(" longblob", " varbinary"))
					.collect(Collectors.joining(", "));
		} else {
			throw new UnsupportedOperationException(
					String.format("Cannot convert data types from: %s, to: %s", from, to));
		}
	}
}
