package com.shchipunov.workshop;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Presents a class that allows to get connections of various databases
 * 
 * @author Shchipunov Stanislav
 */
public class ConnectionFactory {

	/**
	 * Path to the directory where properties files are placed
	 */
	private static final String PATH_TO_PROPERTIES_FILES = "...";

	private static final ConnectionFactory INSTANCE;
	static {
		INSTANCE = new ConnectionFactory();
	}

	private ConnectionFactory() {

	}

	public static ConnectionFactory getInstance() {
		return INSTANCE;
	}

	/**
	 * Allows to get a connection by database product name
	 * 
	 * @param connectionType
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public Connection getConnection(ConnectionType connectionType) throws IOException, SQLException {
		Properties properties = new Properties();
		try (InputStream inputStream = Files
				.newInputStream(Paths.get(PATH_TO_PROPERTIES_FILES, connectionType.toString() + ".properties"))) {
			properties.load(inputStream);
		}
		String url = properties.getProperty("url");
		String user = properties.getProperty("user");
		String password = properties.getProperty("password");
		return DriverManager.getConnection(url, user, password);
	}

	public static enum ConnectionType {
		MYSQL, POSTGRESQL, MICROSOFT_SQL_SERVER;
	}
}
